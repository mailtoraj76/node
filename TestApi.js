
const fs = require('fs')
const fileContents = fs.readFileSync('output.json', 'utf8')
const data = JSON.parse(fileContents)

function getSubjects() {
    
    var uniquesubjects = [] //JSON object array to hold unique subjects
    //Outer loop until end of array
    for (x = 0; x < data.length; x++) {
        // Inner loop for subject details for each unique studen
        for (y = 0; y < data[x].class_details.length; y++) {
            //store subject code and subject description in variable  
            let subjectdetail = {
                "subject_id": data[x].class_details[y].subject_code,
                "subject_name": data[x].class_details[y].subject_desc
            }
            // rval to contain negative if subject code in not available in arry
            let rval = uniquesubjects.findIndex(function (id) 
            { return id.subject_id == data[x].class_details[y].subject_code })
            
            //if subject code in not available in array rval contains negative
            //push the subject code and description into uniquesubjects JSON array
            if (rval < 0) {
                uniquesubjects.push(subjectdetail)
                //console.log(rval)
            }

        }
    }
    //console.log(typeof uniquesubjects)
    //console.log(data.class_details.key())
    return (uniquesubjects)
    //console.log(uniquesubjects.length)


}

//--------------------------------------------unique student given a subject code -----------
function getUniqueStudent(std) {
    //initialize the uniquestudents array
    let uniquestudents = []
    //Start outer loop ...
    for (x = 0; x < data.length; x++) {
        //here capture the student id available in outer loop 
        let studentid = { "student_id": data[x].student_id }
        //Start inner loop to match the subject in question (std)

        for (y = 0; y < data[x].class_details.length; y++) {
            //if the subject in question matches the with value of subject code
            // then check if the same value already exists in the array, if does not 
            // exist then push the student id in the unique array list. 
            if (data[x].class_details[y].subject_code == std) {
                let rval = uniquestudents.findIndex(function (id) 
                { return id.student_id == data[x].student_id })
                
                if (rval < 0) uniquestudents.push(studentid)
                }
        }
    }
    return uniquestudents
}

module.exports.getUniqueStudent = getUniqueStudent
module.exports.getSubjects = getSubjects
