const express = require('express')
const app = express()
var modstd = require('./TestApi')

const cors = require('cors')
app.use(cors())


app.get('/subjects', (req, res) => {
    let sub = modstd.getSubjects()
    if (sub.isNull)
        res.status(400).send ('No Subjects')
    else
        res.status(200).send(sub)
})

app.get('/subjects/:id', (req, res) => {
    let arr = modstd.getUniqueStudent(req.params.id)
    if (arr.length==0)
        res.status(404).send('Sorry!! the given subject code ' + req.params.id + ' not found')
    else
        res.status(200).send(arr)
})
app.listen(3000, () => console.log('server listening for user request'))